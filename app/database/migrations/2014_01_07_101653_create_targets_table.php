<?php

use Illuminate\Database\Migrations\Migration;

class CreateTargetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $config = Config::get('targets');
        
        Schema::create($config['tableName'], function($table) use ($config) {
            $table->increments('id');
            
            $table->integer('user_id')->unsigned(10);
            $table->foreign('user_id')->references('id')->on('users')
                    ->onDelete('cascade')->onUpdate('cascade');
            
            $table->string('name', $config['nameMaxLength']);
            $table->text('description');
            $table->date('date_start');
            $table->date('date_finish');
            $table->boolean('autostart');
            $table->boolean('active');
            $table->boolean('archived');
            $table->string('parameters', $config['tartgetParametersLength']);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        $config = Config::get('targets');
        
		Schema::drop($config['tableName']);
	}

}