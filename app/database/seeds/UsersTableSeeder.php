<?php

/**
 * remember-calories.com (c) 2011-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 */

class usersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users_groups')->delete();

        $user = Sentry::createUser([
            'email'     => 'ypeskov@pisem.net',
            'password'  => '123',
            'activated' => true,
            'first_name'=> 'Yuriy',
            'last_name' => 'Peskov',
        ]);
        //find users group
        $usersGroup = Sentry::findGroupByName('users');
        //add a user to users group
        $user->addGroup($usersGroup);
        
        $user = Sentry::createUser([
            'email'     => 'yuriy.peskov@gmail.com',
            'password'  => '123',
            'activated' => false,
            'first_name'=> 'Yuriy2',
            'last_name' => 'Peskov2',
        ]);
        
        $user->addGroup($usersGroup);
    }
}