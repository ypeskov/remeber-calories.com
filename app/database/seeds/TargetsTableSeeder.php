<?php

/**
 * remember-calories.com (c) 2010-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 * @author Eugene Zhurakhovskiy <txc.work@gmail.com>
 */

class TargetsTableSeeder extends Seeder
{
    public function run()
    {
        $config = Config::get('targets');
        $user = Sentry::findUserByLogin('ypeskov@pisem.net');
        
        DB::table($config['tableName'])->delete();
        
        $target = new Target();
        
        $target->user_id    = $user->id;
        $target->name       = 'Get -10kg by 15 February 2014';
        $target->description= 'I want to get -10kg at that time!';
        $target->date_start = '2014-01-06';
        $target->date_finish= '2014-02-15';
        $target->autostart  = false;
        $target->active     = true;
        $target->archived   = false;
        $target->parameters = json_encode([
            'unit'          => 'kg',
            'startWeight'   => '90',
            'endWeight'     => '80',
        ]);
        $target->save();
        
        $target2 = new Target();
        
        $target2->user_id       = $user->id;
        $target2->name          = 'Get -10kg by 15 April 2014';
        $target2->description   = 'I want to get -10kg at that time!';
        $target2->date_start    = '2014-01-11';
        $target2->date_finish   = '2014-02-15';
        $target2->autostart     = true;
        $target2->active        = false;
        $target2->archived      = false;
        $target2->parameters    = json_encode([
            'unit'          => 'kg',
            'startWeight'   => '80',
            'endWeight'     => '70',
        ]);
        $target2->save();
        
        $target3 = new Target();
        
        $target3->user_id       = $user->id;
        $target3->name          = 'Get -10kg by 15 December 2013';
        $target3->description   = 'I want to get -10kg at that time!';
        $target3->date_start    = '2013-11-11';
        $target3->date_finish   = '2013-12-15';
        $target3->autostart     = true;
        $target3->active        = false;
        $target3->archived      = true;
        $target3->parameters    = json_encode([
            'unit'          => 'kg',
            'startWeight'   => '90',
            'endWeight'     => '80',
        ]);
        $target3->save();
    }
}