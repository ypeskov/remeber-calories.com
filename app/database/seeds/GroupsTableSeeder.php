<?php

/**
 * remember-calories.com (c) 2011-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 */

class GroupsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('groups')->delete();
        
        Sentry::createGroup([
            'name'          => 'users',
            'permissions'   => [
                'admin'         => 0,
                'users'         => 1,
                'superadmins'   => 0,
            ],
        ]);
        
        Sentry::createGroup([
            'name'          => 'admins',
            'permissions'   => [
                'admin'         => 1,
                'users'         => 1,
                'superadmins'   => 0,
            ],
        ]);
        
        Sentry::createGroup([
            'name'          => 'superadmins',
            'permissions'   => [
                'admin'         => 1,
                'users'         => 1,
                'superadmins'   => 1,
            ],
        ]);
    }
}