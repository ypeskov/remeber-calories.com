<?php

Route::get('stuff/addUser', [   
    'uses' => 'StuffController@addUser', 
    'as' => 'stuffAddUser', 
]);

Route::get('stuff/addGroup', [   
    'uses' => 'StuffController@addGroup', 
    'as' => 'stuffAddGroup', 
]);