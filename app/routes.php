<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/', [
    'as'        => 'home',
    'uses'  => 'HomeController@indexAction',
]);

Route::get('login', [
    'as'    => 'login',
    'uses'  => 'UserController@loginAction',
]);

Route::get('logout', [
    'as'    => 'logout',
    'uses'  => 'UserController@logoutAction',
]);

Route::post('login', [
    'as'    => 'loginPost',
    'uses'  => 'UserController@identifyUser'
]);

Route::get('targets', [
    'as'    => 'targets',
    'uses'  => 'TargetsController@indexAction',
]);

Route::post('targets', [
    'as'    => 'addTargetCtrl',
    'uses'  => 'TargetsController@newTargetAction',
]);

Route::get('progress', [
    'as'    => 'progress',
    'uses'  => 'ProgressController@indexAction'
]);

Route::get('advices', [
    'as'    => 'advices',
    'uses'  => 'AdvicesController@indexAction',
]);

Route::get('bookmarks', [
    'as'    => 'bookmarks',
    'uses'  => 'BookmarksController@indexAction'
]);

Route::get('settings', [
    'as'    => 'settings',
    'uses'  => 'SettingsController@indexAction',
]);




//------------------  Temporary routes which are required for development ------
require_once('stuffRoutes.php');