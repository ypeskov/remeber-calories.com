<?php

namespace RememberCalories\Rest;

use Mockery\CountValidator\Exception;

class MyJsonResponse implements MyJsonResponseInterface
{
    protected $success;
    protected $responseCode;
    protected $responseMsg;
    protected $data;

    protected $responseStatuses;

    public function __construct($data=[], $responseCode=0, $responseMsg='')
    {
        $this->data         = $data;
        $this->responseCode = $responseCode;
        $this->responseMsg  = $responseMsg;

        $this->responseStatuses = \Config::get('rest');

        $this->updateSuccess();
    }

    /**
     * Updates the current status of @success property according
     * to @responseStatus
     */
    protected function updateSuccess()
    {
        if ( $this->responseCode === $this->responseStatuses['SUCCESS'] ) {
            $this->success = true;
        } else {
            $this->success = false;
        }

        return $this;
    }

    public function update($data, $responseCode=0, $responseMsg='')
    {
        $this->data         = $data;
        $this->responseCode = $responseCode;
        $this->responseMsg  = $responseMsg;
        $this->updateSuccess();

        return $this;
    }

    public function  setData($data)
    {
        $this->data = $data;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setResponseCode($responseCode)
    {
        $this->responseCode = $responseCode;

        if ( $this->responseCode !== 0 ) {
            $this->success = false;
        }

        return $this;
    }

    public function getResponseCode()
    {
        return $this->responseCode;
    }

    public function setResponseMsg($responseMsg)
    {
        $this->responseMsg = $responseMsg;

        return $this;
    }

    public function getResponseMsg()
    {
        return $this->responseMsg;
    }

    /**
     * Returns a JSON array of for response.
     *
     * @return string
     */
    protected function buildResponseStructure()
    {
        $response = \json_encode([
            'success'       => $this->success,
            'responseCode'  => $this->responseCode,
            'responseMsg'   => $this->responseMsg,
            'data'          => $this->data,
        ]);

        return $response;
    }

    public function getResponse()
    {
        return $this->buildResponseStructure();
    }

    public function getStructure()
    {
        return [
            'success'       => $this->success,
            'responseCode'  => $this->responseCode,
            'responseMsg'   => $this->responseMsg,
            'data'          => $this->data,
        ];
    }

    public function getResponseStatus($codeId)
    {
        if ( isset($this->responseStatuses[$codeId]) ) {
            return $this->responseStatuses[$codeId];
        } else {
            throw new Exception('Invalid response status');
        }
    }

    public function __toString()
    {
        return $this->buildResponseStructure();
    }
}