<?php

namespace RememberCalories\Rest;

interface MyJsonResponseInterface
{
    public function getResponse();

    public function getStructure();

    public function update($data, $responseCode=0, $responseMsg='');
}