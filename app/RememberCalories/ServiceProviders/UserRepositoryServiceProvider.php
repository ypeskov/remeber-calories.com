<?php
/**
 * remember-calories.com (c) 2010-2014
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 2/6/14
 * Time: 7:26 AM
 */

namespace RememberCalories\ServiceProviders;

use Illuminate\Support\ServiceProvider;
use RememberCalories\Repository\UserEloquentRepository;

class UserRepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('RememberCalories\Repository\UserRepository', function($app) {
            return new UserEloquentRepository();
        });
    }
}