<?php
/**
 * remember-calories.com (c) 2010-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 * @author Eugene Zhurakhovskiy <txc.work@gmail.com>
 */

namespace RememberCalories\ServiceProviders;

use \Illuminate\Support\ServiceProvider;
use \RememberCalories\Repository\TargetEloquentRepository;

class TargetRepositoryInterfaceServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('RememberCalories\Repository\TargetRepositoryInterface', function($app, $parameters) {
            if ( isset($parameters['target']) ) {
                $target = $parameters['target'];
            } else {
                $target = new \Target();
            }

            return new TargetEloquentRepository($target);
        });

        //$this->app->bind('TargetRepository', '\RememberCalories\Repository\TargetEloquentRepository');
    }
}