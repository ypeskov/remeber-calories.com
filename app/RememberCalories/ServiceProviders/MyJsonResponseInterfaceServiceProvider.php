<?php

namespace RememberCalories\ServiceProviders;

use \Illuminate\Support\ServiceProvider;
use \RememberCalories\Rest\MyJsonResponse;

class MyJsonResponseInterfaceServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('RememberCalories\Rest\MyJsonResponseInterface', function($app, $parameters) {
            return new MyJsonResponse(null);
        });

//        \App::bind('MyJsonResponseInterface', 'MyJsonResponse');
    }
}