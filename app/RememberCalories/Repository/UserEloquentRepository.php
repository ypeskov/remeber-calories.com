<?php
/**
* remember-calories.com (c) 2010-2014
*
* Author: Yuriy Peskov
* Email: yuriy.peskov@gmail.com
* Date: 2/6/14
* Time: 7:21 AM
*/

namespace RememberCalories\Repository;

use Cartalyst\Sentry\Users\Eloquent\User;

class UserEloquentRepository extends User
{
    protected  $user;

    public function __construct(User $user=null)
    {
        if ( $user !== null ) {
            $this->user = $user;
        }
    }

    public function updateUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getEmptyUser()
    {
        $emptyUser = new \stdClass();

        $emptyUser->id = null;
        $emptyUser->first_name = '';
        $emptyUser->last_name = '';
        $emptyUser->email = '';
        $emptyUser->identified = false;

        return $emptyUser;
    }

    public function getArray()
    {
        return [
            'id'            => $this->user->id,
            'first_name'    => $this->user->first_name,
            'last_name'     => $this->user->last_name,
            'email'         => $this->user->email,
            'activated'     => $this->user->activated,
            'last_login'    => $this->user->last_login,
            'created_at'    => $this->user->created_at,
            'updated_at'    => $this->user->updated_at,
            'identified'    => ( $this->user->id != null ) ? true : false,
        ];
    }
}