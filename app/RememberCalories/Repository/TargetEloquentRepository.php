<?php
/**
 * remember-calories.com (c) 2010-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 * @author Eugene Zhurakhovskiy <txc.work@gmail.com>
 */

namespace RememberCalories\Repository;

class TargetEloquentRepository implements TargetRepositoryInterface
{
    protected $target;

    /**
     * If target is passed use it, otherwise create a new instance
     * 
     * @param \Target $target
     */
    public function __construct(\Target $target)
    {
        $this->target = $target;
    }
    
    public function all() 
    {
        return $this->target->all();
    }
    
    public function find($id)
    {
        return $this->target->find($id);
    }
    
    /**
     * Returns a collection of all active targets.
     * 
     * @param Integer $userId
     * @param String $orderByActive
     * 
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getAllActiveForUser($userId, $orderByActive = 'desc')
    {
        $ret = $this->target
            ->where('user_id', '=', $userId)
            ->where('archived', '!=', true)
            ->orderBy('active', $orderByActive)
            ->get();

        return $ret;
    }

    /**
     * Returns all targets for definite user.
     *
     * @param Interger $userId
     * @param string $orderByActive
     * @return mixed
     */
    public function getAllForUser($userId, $orderByActive = 'desc')
    {
        return $this->target
            ->where('user_id', '=', $userId)
            ->orderBy('active', $orderByActive)
            ->get();
    }

    /**
     * Validates the target.
     *
     * @param Array $target
     * @return array
     */
    public function validateTarget(Array $target)
    {
        $errors = [];

        $validator = \Validator::make($target, [
            'name'          => 'required | min: 5 | max: 100',
            'description'   => '',
            'date_start'    => 'required',
            'date_finish'   => 'required',
        ]);

        if ( $validator->fails() ) {
            $messages = $validator->messages();

            $isValid    = false;
            $errors     = $messages->all();
        } else {
            $isValid    = true;
        }

        return [
            'isValid'   => $isValid,
            'errors'    => $errors,
        ];
    }

    /**
     * Adds a new target to DB.
     * TODO:  think about processing an error
     *
     * @param array $target
     * @return array
     */
    public function add(Array $target)
    {
        $this->target->name         = $target['name'];
        $this->target->user_id      = $target['user_id'];
        $this->target->description  = isset($target['description']) ? $target['description'] : '';
        $this->target->date_start   = $target['date_start'];
        $this->target->date_finish  = $target['date_finish'];

        try {
            $this->target->save();
            $target = $this->target->toArray();
        } catch(\Exception $e) {
            var_dump($e->getMessage());
            die();
        }

        return $target;
    }
}