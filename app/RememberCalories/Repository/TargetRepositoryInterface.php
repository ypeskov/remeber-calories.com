<?php
/**
 * remember-calories.com (c) 2010-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 * @author Eugene Zhurakhovskiy <txc.work@gmail.com>
 */

namespace RememberCalories\Repository;

interface TargetRepositoryInterface
{
    public function all();
    
    public function find($id);
    
    public function getAllActiveForUser($userId);
    
    public function getAllForUser($userId);

    public function add(Array $target);

    public function validateTarget(Array $target);
}