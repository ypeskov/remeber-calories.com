<?php

namespace RememberCalories\MainMenu;

Interface MainMenuInterface
{
    public function build();
}