<?php

namespace RememberCalories\MainMenu;

/**
 * This class is responsible for building the structure of the main menu.
 * The structure is passed later to view for building HTML.
 * Config for the menu is located in /app/config/main_menu.php
 */
class MainMenu implements MainMenuInterface
{
    protected $menu;
    
    public function __construct()
    {
        $this->menu = \Config::get('main_menu');
    }
    
    /**
     * Builds Array for main menu. According to the current route make one item active.
     * 
     * @return Array
     */
    public function build()
    {
        $currentRoute = \Route::currentRouteName();
        $menu = [];
        
        $i = 0;
        foreach($this->menu as $menuItem) {
            
            $menu[$i]['active'] = ($currentRoute == $menuItem['route']) ? 'active' : '';
            
            $menu[$i]['route'] = \URL::route($menuItem['route']);
            
            if ( $menuItem['isPlural'] ) {
                $menu[$i]['name'] = \mb_ucfirst(\Lang::choice('pages.' . $menuItem['nameKey'], 2));
            } else {
                $menu[$i]['name'] = \mb_ucfirst(\Lang::get('pages.' . $menuItem['nameKey']));
            }
            
            $i++;
        }
        
        return $menu;
    }
}