<?php

namespace RememberCalories\Filters;

/**
 * Filter for getting information whether a user is logged in and activated.
 * Stores results to session:
 * user.userId  ID
 * user.activated Boolean
 */
class AccessFilter
{
    /**
     * Entry point for the filter.
     */
    public function filter()
    {
        $userId = null;
        
        if ( \Sentry::check() ) {
            $userId = \Session::get('cartalyst_sentry', [])[0];
            
            if ( $userId ) {
                $this->storeUserToSession($userId);
            }
        }
    }
    
    /**
     * Stores information to the session.
     * 
     * @param Integer $userId
     * @throws \RememberCalories\Filters\UserNotFoundException
     */
    protected function storeUserToSession($userId)
    {
        try {
            $user = \Sentry::findUserById($userId);
                        
            \Session::put('user.userId', $user->id);
            \Session::put('user.activated', $user->activated);
            \Session::put('user.first_name', $user->first_name);
            \Session::put('user.last_name', $user->last_name);
            
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            throw $e;
        }
        
        return true;
    }
}