@extends('layouts.main')

@section('content')
<div id="targets_wrapper">
    <div id="current_targets_wrapper">
        <p id="current_targets_heading"><?= Lang::get('pages.current_targets'); ?></p>
        @if ($targets->count() > 0 )
            <table id="current_targets_table">
                <thead>
                    <th><?= mb_ucfirst(trans('pages.order')); ?></th>
                    <th><?= mb_ucfirst(trans('pages.date_start')); ?></th>
                    <th><?= mb_ucfirst(trans('pages.date_finish')); ?></th>
                    <th><?= mb_ucfirst(trans('pages.autostart')); ?></th>
                    <th><?= mb_ucfirst(trans('pages.name')); ?></th>
                    <th><?= mb_ucfirst(trans('pages.is_active')); ?></th>
                </thead>
                @foreach($targets as $index => $target)
                <tr>
                    <td>{{ $index + 1 }}</td>
                    <td>{{ $target['date_start'] }}</td>
                    <td>{{ $target['date_finish'] }}</td>
                    <td>
                        @if ( $target['autostart'] )
                        <?= mb_ucfirst(trans('pages.enabled')); ?>
                        @else
                        <?= mb_ucfirst(trans('pages.disabled')); ?>
                        @endif
                    </td>
                    <td>{{ $target['name'] }}</td>
                    <td>
                        @if ( $target['active'] )
                        <?= mb_ucfirst(trans('pages.yes')); ?>
                        @else
                        <?= mb_ucfirst(trans('pages.no')); ?>
                        @endif
                    </td>
                </tr>
                @endforeach
            </table>
        @else
            <p><?= trans('pages.no_current_targets_available'); ?></p>
        @endif
    </div>
</div>
@stop