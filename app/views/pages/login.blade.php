@extends('layouts.main')

@section('content')
@parent
<div>
    <form name="login_form" method="post" action="<?= URL::route('loginPost') ?>">
        <div>
            <label for="login_email">Email:</label><input id="login_email" type="email" name="email" />
        </div>
        <div>
            <label for="login_password">Password:</label><input id="login_password" type="password" name="password" />
        </div>
        <div>
            <input type="submit" value="Log in" />
        </div>
    </form>
</div>
@stop