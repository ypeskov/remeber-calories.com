<div id="menu_container">
    <ul>
        @foreach($menu as $menuItem)
        <li>
            <a href="<?= $menuItem['route'] ?>" class="main_menu_item <?= $menuItem['active'] ?>">
                <?= $menuItem['name'] ?>
            </a>
        </li>
        @endforeach
        
    </ul>
</div>