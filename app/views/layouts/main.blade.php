<!doctype html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>{{ $title }}</title>
    
    <link rel="stylesheet" href="/css/style.css" />
</head>

<body>
    <div id="wrapper">
        <div id="header">
            <div>
                <a href="/"><img src="/images/logo.png" alt="logo.png" /></a>
            </div>
            
            <div id="main_menu">
                @include('components.main_menu')
            </div>
        </div>
        
        <div id="main_content">
            @section('content')
            This is Sparta!
            @show
        </div>
        
        <div id="footer">
            
        </div>
    </div>
    

</body>

</html>
