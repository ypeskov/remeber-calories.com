<?php

class AccessFilterTest extends TestCase
{
    /**
     * Test the case when user is logged in.
     */
    public function testAccessFilterWhenUserIsLoggedIn()
    {
        $fakeUserId = 1;
        $accessFilter = new RememberCalories\Filters\AccessFilter();
        
        //prepare mocks for calls
        \Sentry::shouldReceive('check')->once()->andReturn(true);
        \Session::shouldReceive('get')->once()
                ->with('cartalyst_sentry', [])
                ->andReturn([$fakeUserId, 'string']);
        
        $fakeUser = new stdClass();
        $fakeUser->id           = $fakeUserId;
        $fakeUser->activated    = true;
        $fakeUser->first_name   = 'FirstName';
        $fakeUser->last_name    = 'LastName';
        
        \Sentry::shouldReceive('findUserById')
                ->with($fakeUserId)
                ->andReturn($fakeUser);
        
        \Session::shouldReceive('put')->andReturn(true);
        
        $accessFilter->filter();
        
    }
}