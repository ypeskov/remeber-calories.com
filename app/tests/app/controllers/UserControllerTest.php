<?php

class UserControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        \Session::start();
    }

    public function testLoginActionUnauthorizedUser()
    {
        $response = $this->action('GET', 'UserController@loginAction');
        $this->assertFalse($response->getData()->data->identified);
    }

    /**
     * TODO: I don't know how to mock Sentry and test this case
     */
    public function testLoginActionAuthorizedUser()
    {

    }
}