<?php

/**
 * remember-calories.com (c) 2011-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 */

class SettingsControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        \Session::start();
    }

    public function testIndexActionAnonymousUser()
    {
        $response = $this->action('GET', 'SettingsController@indexAction');
        $view = $response->original;
        
        $this->assertViewHas('title');
        $this->assertEquals('Change your settings', $view['title']);
        $this->assertResponseOk();
        
        $crawler = $this->client->request('GET', '/settings');
        $this->assertTrue($this->client->getResponse()->isOk());
        $this->assertCount(1, $crawler->filter('h3:contains("Settings for anonymous users")'));
    }
    
    public function testIndexActionNonActivatedUser()
    {
        \Session::put('user.userId', 1);
        
        $crawler = $this->client->request('GET', '/settings');
        
        $this->assertTrue($this->client->getResponse()->isOk());
        $this->assertCount(1, $crawler->filter('h3:contains("Settings for non activated users")'));
    }
    
    public function testIndexActionActivatedUser()
    {
        \Session::put('user.userId', 1);
        \Session::put('user.activated', true);
        
        $crawler = $this->client->request('GET', '/settings');
        
        $this->assertTrue($this->client->getResponse()->isOk());
        $this->assertCount(1, $crawler->filter('h3:contains("Settings for activated users")'));
    }
}