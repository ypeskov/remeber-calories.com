<?php

/**
 * remember-calories.com (c) 2011-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 */

class AdvicesControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        \Session::start();
    }

    public function testIndexActionAnonymousUser()
    {
        $response = $this->action('GET', 'AdvicesController@indexAction');
        $view = $response->original;
        
        $this->assertViewHas('title');
        $this->assertEquals('Useful information for you', $view['title']);
        $this->assertResponseOk();
    }
    
    public function testIndexActionNonActivatedUser()
    {
        \Session::put('user.userId', 1);

        $crawler = $this->client->request('GET', '/advices');

        $this->assertTrue($this->client->getResponse()->isOk());
        $this->assertCount(1, $crawler->filter('h3:contains("Advices for non activated users")'));
    }
    
    public function testIndexActionActivatedUser()
    {
        \Session::put('user.userId', 1);
        \Session::put('user.activated', true);
        
        $crawler = $this->client->request('GET', '/advices');
        
        $this->assertTrue($this->client->getResponse()->isOk());
        $this->assertCount(1, $crawler->filter('h3:contains("Advices for activated users")'));
    }
}