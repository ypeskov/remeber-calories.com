<?php

/**
 * remember-calories.com (c) 2011-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 */

class ProgressControllerTest extends TestCase
{
    public function testIndexAction()
    {
        $response = $this->action('GET', 'ProgressController@indexAction');
        $view = $response->original;
        
        $this->assertViewHas('title');
        $this->assertEquals('Keep abreast of your progress', $view['title']);
        $this->assertResponseOk();
    }
}