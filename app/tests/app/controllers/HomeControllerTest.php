<?php

/**
 * remember-calories.com (c) 2011-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 */

class HomeControllerTest extends TestCase
{
    public function testIndexAction()
    {
        $response = $this->action('GET', 'HomeController@indexAction');
        $view = $response->original;
        
        $this->assertViewHas('title');
        $this->assertEquals('Welcome to remember-calories.com', $view['title']);
        $this->assertResponseOk();
    }
}