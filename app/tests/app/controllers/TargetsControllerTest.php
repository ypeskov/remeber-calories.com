<?php

/**
 * remember-calories.com (c) 2011-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 */

class TargetsControllerTest extends TestCase
{
    public function testIndexActionAnonymousUser()
    {
        $response = $this->action('GET', 'TargetsController@indexAction');

        $this->assertResponseOk();

        $this->assertTrue(empty($response->getData()->data));
    }
    
    public function testIndexActionNonActivatedUser()
    {
        \Session::put('user.userId', 1);

        $response = $this->action('GET', 'TargetsController@indexAction');

        $crawler = $this->client->request('GET', '/targets');
        
        $this->assertTrue($this->client->getResponse()->isOk());
        $this->assertTrue(empty($response->getData()->data));
    }
    
    public function testIndexActionActivatedUser()
    {
        \Session::put('user.userId', 1);
        \Session::put('user.activated', true);

        $response = $this->action('GET', 'TargetsController@indexAction');
        
        $crawler = $this->client->request('GET', '/targets');
        
        $this->assertTrue($this->client->getResponse()->isOk());
    }
}