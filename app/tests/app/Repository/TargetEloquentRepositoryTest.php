<?php
/**
 * remember-calories.com (c) 2010-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 * @author Eugene Zhurakhovskiy <txc.work@gmail.com>
 */

use \RememberCalories\Repository\TargetEloquentRepository;

class TargetEloquentRepositoryTest extends TestCase
{
    /**
     * 
     * @dataProvider providerUserIds
     */
    public function testGetAllActiveForUser($userId, $response, $count)
    {
        $mockCollection = $this->mockCollection($response);
        $mockTarget = $this->mockTarget($userId, $mockCollection);

        //$target = App::build('RememberCalories\Repository\TargetRepositoryInterface', $mockTarget);
        $target = new TargetEloquentRepository($mockTarget);
        
        $result = $target->getAllActiveForUser($userId);
        
        $this->assertEquals('Illuminate\Database\Eloquent\Collection', get_class($result));
        $this->assertCount($count, $result);
    }

    //TODO: Think about testing this method. Problem with mocking.
//    /**
//     *
//     * @dataProvider providerForResponseAndCount
//     */
//    public function testGetAllTargets($response, $count)
//    {
//        $mockTarget = Mockery::mock('Target');
////        $mockTarget->shouldReceive('all')->andReturn($response);
//        $mockTarget->shouldReceive('all')->andReturn(123);
//        var_dump($mockTarget->all());
//        die();
//
//
////        $target = App::make('RememberCalories\Repository\TargetRepositoryInterface', ['target' => $mockTarget]);
//        $target = new TargetEloquentRepository($mockTarget);
//
//        $result = $target->all();
//
//        $this->assertEquals('Illuminate\Database\Eloquent\Collection', get_class($result));
//        $this->assertCount($count, $result);
//    }
    
    
    
    
    //-------------------- MOCKING OBJECTS -----------------------
    
    public function mockCollection($response)
    {
        $mockCollection = Mockery::mock('Collection');

        $mockCollection->shouldReceive('where')
                ->with('archived', '!=', true)
                ->andReturn($mockCollection);
        
        $mockCollection->shouldReceive('orderBy')
                ->with('active', 'desc')
                ->andReturn($mockCollection);
        
        $mockCollection->shouldReceive('get')->andReturn($response);
        
        return $mockCollection;
    }
    
    public function mockTarget($userId, $mockCollection)
    {
        $targetMock = Mockery::mock('Target');
        
        $targetMock->shouldReceive('where')
                ->with('user_id', '=', $userId)
                ->andReturn($mockCollection);
        
        return $targetMock;
    }
    
    
    //-------------------- DATA PROVIDERS -------------------------
    
    public function providerUserIds()
    {
        $response = new Illuminate\Database\Eloquent\Collection();
        $response->add([
            'user_id'       => 1,
            'name'          => 'name1',
            'description'   => 'description1',
            'archived'      => false,
        ]);
        $response->add([
            'user_id'       => 2,
            'name'          => 'name2',
            'description'   => 'description2',
            'archived'      => false,
        ]);

        return [
            [1, $response, $response->count()],
            [2, $response, $response->count()],
            [3, $response, $response->count()],
        ];
    }
    
    public function providerForResponseAndCount()
    {
        $response = new Illuminate\Database\Eloquent\Collection();
        $response->add([
            'user_id'       => 1,
            'name'          => 'name1',
            'description'   => 'description1',
            'archived'      => false,
        ]);
        $response->add([
            'user_id'       => 2,
            'name'          => 'name2',
            'description'   => 'description2',
            'archived'      => false,
        ]);

        return [
            [$response, $response->count()],
            [$response, $response->count()],
            [$response, $response->count()],
        ];
    }
}