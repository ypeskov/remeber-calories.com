<?php

/**
 * remember-calories.com (c) 2011-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 */

class ProgressController extends BaseController
{
    protected $pathToPages      = 'pages.progress';
    protected $indexPageName    = 'progress';

}