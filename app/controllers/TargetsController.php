<?php

use \RememberCalories\Rest\MyJsonResponse;

class TargetsController extends BaseController
{
    protected $pathToPages      = 'pages.targets';
    protected $indexPageName    = 'targets';

    public function indexAction()
    {
        $userId = Session::get('user.userId', null);
        
        return Response::json(
            (new MyJsonResponse($this->target->getAllForUser($userId)->toArray(),
                                $this->myJsonResponse->getResponseStatus('SUCCESS')))
                ->getStructure()
        );
    }

    /**
     * This controller adds a new target to DB.
     *
     * @return JSON
     */
    public function newTargetAction()
    {
        $userId = Session::get('user.userId', null);

        $newTarget = \Input::all('newTarget', [])['newTarget'];
        $newTarget['user_id'] = (int) $userId;

        $validation = $this->target->validateTarget($newTarget);

        if ( $validation['isValid'] === false ) {
            $response =  Response::json(
                (new MyJsonResponse(null,
                            $this->myJsonResponse->getResponseStatus('INVALID_PARAMETERS'),
                            $validation['errors']))
                    ->getStructure()
            );
        } else {
            $target = $this->target->add($newTarget);

            $response = Response::json(
                (new MyJsonResponse($target,
                            $this->myJsonResponse->getResponseStatus('SUCCESS'),
                            'Success'))
                    ->getStructure()
            );
        }

        return $response;
    }
}