<?php

use \RememberCalories\MainMenu\MainMenu;
use \RememberCalories\Repository\TargetRepositoryInterface;
use \RememberCalories\Rest\MyJsonResponseInterface;
use \RememberCalories\Repository\UserEloquentRepository;

class BaseController extends Controller 
{
    protected $viewVars;
    protected $mainMenu;
    
    //Dependency injection classes
    protected $target;
    protected $myJsonResponse;
    protected $userRepository;
    

    public function __construct(TargetRepositoryInterface $target,
                                MyJsonResponseInterface $myJsonResponse,
                                UserEloquentRepository $userRepository)
    {
        $this->beforeFilter('accessFilter');
        
        $this->target           = $target;
        $this->myJsonResponse   = $myJsonResponse;
        $this->userRepository   = $userRepository;
        
        $this->mainMenu = (new MainMenu())->build();
        
        $this->prepareViewVariables();
    }

    /**
     * Prepare the class property $viewVars where will be kept all variables
     * that are common for all controllers.
     */
    protected function prepareViewVariables()
    {
        $this->viewVars = [];
        $this->viewVars['title']    = $this->getPageTitle();
        $this->viewVars['menu']     = $this->mainMenu;
    }
    
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

    /**
     * Returns pages title. If it is not available in language file the current route is rerturned
     * 
     * @return string
     */
    protected function getPageTitle()
    {
        $routeName = Route::currentRouteName();
        
        return trans('pageTitles.' . $routeName);
    }
    
    /**
     * Depending on whether user is logged in and activated display some information on page.
     * 
     * @return View
     */
    public function indexAction()
	{
        $ret = View::make($this->pathToPages . '.'
                            . $this->indexPageName . '_anonymous',
                            $this->viewVars);

        if ( \Session::get('user.userId') ) {
            $user = \Session::get('user');

            if (\Session::get('user.activated') ) {
                $ret = $this->buildActivatedUserView($user);
            } else {
                $ret = View::make($this->pathToPages . '.' . $this->indexPageName . '_not_activated',
                                    $this->viewVars);
            }
        }
        
        return $ret;
	}
    
    /**
     * Build the view of $content section for logged in and activated users.
     * 
     * @param Array $user
     * @return View
     */
    protected function buildActivatedUserView($user)
    {
        return View::make($this->pathToPages . '.' . $this->indexPageName . '_activated', 
                            $this->viewVars);
    }
}