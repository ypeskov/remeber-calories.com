<?php

/**
 * Description of StuffController
 *
 * @author ypeskov
 */
class StuffController extends BaseController{
    public function addUser()
    {
        $email      = Input::get('email');
        $password   = Input::get('password');
        $activated  = Input::get('activated');
        
        try {
            $user = Sentry::createUser([
                'email'     => $email,
                'password'  => $password,
                'activated' => $activated,
            ]);
            
            $usersGroup = Sentry::findGroupById(1);
            
            $user->addGroup($usersGroup);
            
            echo "The user has been created";
            var_dump($user);
            
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    
    public function addGroup()
    {
        $name = Input::get('name');
        
        try {
            $group = Sentry::createGroup([
                'name'          => $name,
                'permissions'   => [
                    'admin'     => 0,
                    'users'     => 1,
                ],
            ]);
            
            echo "The group was created:";
            var_dump($group);
            
        } catch (Cartalyst\Sentry\Groups\NameRequiredException $e) {
            echo $e->getMessage();
        } catch (Cartalyst\Sentry\Groups\GroupExistsException $e) {
            echo $e->getMessage();
        }
    }
}
