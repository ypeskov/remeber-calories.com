<?php

class UserController extends BaseController
{
    /**
     * check whether a user is logged in
     * 
     * @return JSON
     */
    public function loginAction()
    {
        if ( Sentry::check() ) {
            $this->userRepository   = $this->userRepository->updateUser(Sentry::getUser());
            $ret = $this->myJsonResponse->update($this->userRepository->getArray())->getStructure();
        } else {
            $ret = $this->myJsonResponse->update($this->userRepository->getEmptyUser())->getStructure();
        }

        return Response::json($ret);
    }
   
    /**
     * Clears the session and redirects a user to page where he was before.
     * 
     * @return Redirect
     */
    public function logoutAction()
    {
        Sentry::logout();
        Session::clear();
        
        return Response::json($this->myJsonResponse->getStructure());
    }
    
    /**
     * The method tries to login into account.
     * 
     * @return type
     * @throws InvalidArgumentException
     */
    public function identifyUser()
    {
        try {
            if ( !Input::has('email') or !Input::has('password')) {
                throw new InvalidArgumentException('email and password are obligatory fields');
            }

            $credentials = [
                'email'     => Input::get('email'),
                'password'  => Input::get('password'),
            ];

            //try to identify user and login if found
            $user = Sentry::authenticate($credentials);
            Sentry::login($user);

            $this->userRepository   = $this->userRepository->updateUser($user);

            return $this->myJsonResponse->update($this->userRepository->getArray())->getStructure();
            
        } catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
            echo 'Login field is required.';
        } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
            echo 'Password field is required.';
        } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
            return $this->myJsonResponse
                ->update($this->userRepository->getEmptyUser(),
                         $this->myJsonResponse->getResponseStatus('INVALID_LOGIN'))
                ->getStructure();
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return $this->myJsonResponse
                ->update($this->userRepository->getEmptyUser(),
                         $this->myJsonResponse->getResponseStatus('INVALID_LOGIN'))
                ->getStructure();
        } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            echo 'User is not activated.';
        } catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
            echo 'User is suspended.';
        } catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {
            echo 'User is banned.';
        }
    }

}