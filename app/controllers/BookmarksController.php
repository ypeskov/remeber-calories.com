<?php

/**
 * remember-calories.com (c) 2011-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 */

class BookmarksController extends BaseController
{
    protected $pathToPages      = 'pages.bookmarks';
    protected $indexPageName    = 'bookmarks';
}