<?php

/**
 * remember-calories.com (c) 2011-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 */

class AdvicesController extends BaseController
{
    protected $pathToPages      = 'pages.advices';
    protected $indexPageName    = 'advices';
}