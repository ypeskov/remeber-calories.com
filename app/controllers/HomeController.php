<?php



class HomeController extends BaseController 
{
    protected $pathToPages      = 'pages.welcome';
    protected $indexPageName    = 'welcome';


    public function indexAction()
    {
        $currentLocale = \Input::get('locale', 'en-us');

        require('info.html');
    }
}