<?php

return [
    'SUCCESS'               => 0,

    'INVALID_PARAMETERS'    => 1000,

    'NOT_AUTHENTICATED'     => 2000,
    'INVALID_LOGIN'         => 2100,
];