<?php

/**
 * remember-calories.com (c) 2011-2014
 * @link http://remember-calories.com 
 * @author Yuriy Peskov <yuriy.peskov@gmail.com>
 */

return [
    'tableName'                 => 'targets',
    'nameMaxLength'             => 100,
    'descriptionMaxLength'      => 500,  //not used currently
    'tartgetParametersLength'   => 1000,
    
];