<?php

return [
    'welcome_title'     => 'welcome to remember-calories.com',
    'dashboard'         => 'dashboard',
    'target'            => 'target|targets',
    'progress'          => 'progress',
    'advice'            => 'advice|advices',
    'my_bookmarks'      => 'my bookmarks',
    'setting'           => 'settings',
    'greeting_welcome_back'=> 'Welcome back, <strong>:firstName :lastName</strong>.',
    'current_targets'   => 'Current targets:',
    
    'order'         => 'order',
    'date_start'    => 'start Date',
    'date_finish'   => 'finish Date',
    'autostart'     => 'autostart',
    'name'          => 'name',
    'is_active'     => 'active',
    'enabled'       => 'enabled',
    'disabled'      => 'disabled',
    'yes'           => 'yes',
    'no'            => 'no',
];