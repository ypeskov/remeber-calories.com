<?php
return [
    'home'      => 'Welcome to remember-calories.com',
    'login'     => 'Login into your account',
    'targets'   => 'Manage your targets',
    'progress'  => 'Keep abreast of your progress',
    'advices'   => 'Useful information for you',
    'bookmarks' => 'Quick access to your places',
    'settings'  => 'Change your settings',
];