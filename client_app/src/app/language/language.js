/**
 * remember-calories.com (c) 2010-2014
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 2/20/14
 * Time: 9:21 AM
 */

angular.module('app.language', [])

.config([function() {
    "use strict";


}])

.controller('LanguageCtrl', ['$scope', 'LanguageService', '$locale', function($scope, LanguageService, $locale) {
    "use strict";

    var _self = this;

    _self.locales = [
        {id: 'en-us', label: 'English'},
        {id: 'ru', label: 'Russian'}
    ];

    _self.currentLocale = LanguageService.getCurrentLocale($locale.id, _self.locales);

    _self.changeLocale = function()
    {
        LanguageService.changeLocale(_self.currentLocale);
    };

    return $scope.LanguageCtrl = _self;
}])

.factory('LanguageService', ['$http', function($http) {
    "use strict";

    return {
        changeLocale: function(newLanguage) {
            var newLocale = newLanguage['id'];

            window.location.href = '/?locale=' + newLocale;
        },

        getCurrentLocale: function(currentLocaleId, allLocales) {
            for(var i=0; i < allLocales.length; i++) {
                if ( allLocales[i].id === currentLocaleId ) {
                    return allLocales[i];
                }
            }

            alert('Unknown locale');

            throw new Exception('Error unknown locale');
        }
    };
}])

;