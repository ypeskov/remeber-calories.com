angular.module('app.topmenu', [])

.config([function() {

}])

.controller('TopmenuCtrl', ['$scope', 'TOP_MENU_CONFIG', function($scope, TOP_MENU_CONFIG) {
        $scope.menu = angular.copy(TOP_MENU_CONFIG.items);
}])

/**
 * Order of menu items is strongly set here. In case to change the order, edit
 * Array "items", otherwise tests will fail.
 */
.constant('TOP_MENU_CONFIG', {
    items: [
        {id: "dashboard", label: "Dashboard",   path: "/dashboard", order: 0},
        {id: "targets",   label: "Targets",     path: "/targets",   order: 1},
        {id: "progress",  label: "Progress",    path: "/progress",  order: 2},
        {id: "advices",   label: "Advices",      path: "/advices",   order: 3},
        {id: "bookmarks", label: "Bookmarks",   path: "/bookmarks", order: 4},
        {id: "settings",  label: "Settings",    path: "/settings",  order: 5}
    ]
})

;