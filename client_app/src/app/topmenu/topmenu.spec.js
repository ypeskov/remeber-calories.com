describe('top menu section', function() {
    beforeEach(module('app.topmenu'));
    
    it('Should contain correct menu constant paths', inject(function(TOP_MENU_CONFIG) {
        expect(TOP_MENU_CONFIG.items[0].path).toBe('/dashboard');
        expect(TOP_MENU_CONFIG.items[1].path).toBe('/targets');
        expect(TOP_MENU_CONFIG.items[2].path).toBe('/progress');
        expect(TOP_MENU_CONFIG.items[3].path).toBe('/advices');
        expect(TOP_MENU_CONFIG.items[4].path).toBe('/bookmarks');
        expect(TOP_MENU_CONFIG.items[5].path).toBe('/settings');
    }));
    
    it('Should contain correct menu constant ids', inject(function(TOP_MENU_CONFIG) {
        expect(TOP_MENU_CONFIG.items[0].id).toBe('dashboard');
        expect(TOP_MENU_CONFIG.items[1].id).toBe('targets');
        expect(TOP_MENU_CONFIG.items[2].id).toBe('progress');
        expect(TOP_MENU_CONFIG.items[3].id).toBe('advices');
        expect(TOP_MENU_CONFIG.items[4].id).toBe('bookmarks');
        expect(TOP_MENU_CONFIG.items[5].id).toBe('settings');
    }));
    
    it('Should contain correct menu items order', inject(function(TOP_MENU_CONFIG) {
        expect(TOP_MENU_CONFIG.items[0].order).toBe(0);
        expect(TOP_MENU_CONFIG.items[1].order).toBe(1);
        expect(TOP_MENU_CONFIG.items[2].order).toBe(2);
        expect(TOP_MENU_CONFIG.items[3].order).toBe(3);
        expect(TOP_MENU_CONFIG.items[4].order).toBe(4);
        expect(TOP_MENU_CONFIG.items[5].order).toBe(5);
    }));
});