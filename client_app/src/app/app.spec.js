describe('AppCtrl different states', function () {
    var AppCtrl, $location, $scope;
    var menuItemLength = 4; //the length of items for each top menu

    beforeEach(module('app'));

    beforeEach(inject(function ($controller, _$location_, $rootScope, $state, $log) {
        $location = _$location_;
        $scope = $rootScope.$new();

        AppCtrl = $controller('AppCtrl', {
            $scope: $scope,
            $location: $location,
            $log: $log
        });
    }));

    //----------------------- Test paths of states ------------------------------------------
    it('Run tests on state "dashboard".', inject(function ($state, $rootScope) {
        $state.go('dashboard', {
            locale: 'en'
        });
        $rootScope.$apply();

        expect($scope.AppCtrl.menuItem.path).toBe('/dashboard');
        expect(Object.keys($scope.AppCtrl.menuItem).length).toBe(menuItemLength);
    }));

    it('Run tests on state "targets".', inject(function ($state, $rootScope) {
        $state.go('targets', {});
        $rootScope.$apply();

        expect($scope.AppCtrl.menuItem.path).toBe('/targets');
        expect(Object.keys($scope.AppCtrl.menuItem).length).toBe(menuItemLength);
    }));

    it('Run tests on state "progress".', inject(function ($state, $rootScope) {
        $state.go('progress', {});
        $rootScope.$apply();

        expect($scope.AppCtrl.menuItem.path).toBe('/progress');
        expect(Object.keys($scope.AppCtrl.menuItem).length).toBe(menuItemLength);
    }));

    it('Run tests on state "advices".', inject(function ($state, $rootScope) {
        $state.go('advices', {});
        $rootScope.$apply();

        expect($scope.AppCtrl.menuItem.path).toBe('/advices');
        expect(Object.keys($scope.AppCtrl.menuItem).length).toBe(menuItemLength);
    }));

    it('Run tests on state "bookmarks".', inject(function ($state, $rootScope) {
        $state.go('bookmarks', {});
        $rootScope.$apply();

        expect($scope.AppCtrl.menuItem.path).toBe('/bookmarks');
        expect(Object.keys($scope.AppCtrl.menuItem).length).toBe(menuItemLength);
    }));

    it('Run tests on state "settings".', inject(function ($state, $rootScope) {
        $state.go('settings', {});
        $rootScope.$apply();

        expect($scope.AppCtrl.menuItem.path).toBe('/settings');
        expect(Object.keys($scope.AppCtrl.menuItem).length).toBe(menuItemLength);
    }));

});
