angular.module('app.user', [])

.constant('USER_CONFIG', {
    loginUrl:       '/login',
    logoutUrl:      '/logout'
})

.config([function() {

}])

.controller('UserCtrl',
    ['$scope', '$http', 'user', 'USER_CONFIG', 'Dashboard', '$state', 'Data',
      '$stateParams',
        function($scope, $http, user, USER_CONFIG, Dashboard, $state, Data, $stateParams) {
    "use strict";
    var _self = this;

    _self.firstTime = true;
    _self.displayLoginError = false;

    if ( typeof _self.user === 'undefined') {
        _self.user = {};
        _self.user.id = 0;

        user.checkAuth().then(
            function(data)
            {
                _self.user = Data.user = data;

                if ( data.id == null ) {
                    $state.go('dashboard.anonymous', {
                        locale: $stateParams.locale
                    });
                } else {
                    _self.displayLoginError = false;
                    $state.go('dashboard.user', {
                        locale: $stateParams.locale
                    });
                }
            },

            function(data)
            {
                alert(data);
            }
        );
    }

    _self.tryToLogin    = user.login(_self);
    _self.logout        = user.logout(_self);

    return $scope.UserCtrl = _self;
}])

.factory('user', ['$q', '$http', 'USER_CONFIG', '$state', 'Data',
        function($q, $http, USER_CONFIG, $state, Data) {
    "use strict";
    var _self = this;

    /**
     * The function which returns the name for state which should
     * be applied after login to an account.
     *
     * @returns {string}
     */
    function getStateAfterLogin()
    {
        return 'dashboard.user';
    }

    return {
        /**
         * Checking authorization on server for current user.
         *
         * @returns {Promise.promise|*}
         */
        checkAuth: function() {
            var deferred = $q.defer();

            $http.get(USER_CONFIG.loginUrl, {})
            .success(function(data, status, headers, config) {
                if ( data.success === true ) {
                    deferred.resolve(data.data);
                } else {
                    deferred.reject(data.data);
                }
            })
            .error(function(data, status, headers, config) {
                deferred.reject(data);
            });

            return deferred.promise;
        },

        /**
         * Function for logging into account
         * @param scope
         * @returns {Function}
         */
        login:  function(scope) {

            return function() {
                $http
                    .post(USER_CONFIG.loginUrl, {
                        email:  scope.user.email,
                        password: scope.user.password
                    })
                    .success(function(data, status, headers, config) {
                        if ( data.success === true ) {
                            scope.displayLoginError = false;
                            scope.user = Data.user = data.data;

                            $state.go(getStateAfterLogin());
                        } else {
                            scope.displayLoginError = true;
                            scope.user = Data.user = data.data;
                        }
                    })
                    .error(function(data, status, headers, config) {
                        alert('Error while trying to login');
                    });
            };
        },

        /**
         * Logout from the account.
         *
         * @param scope
         * @returns {Function}
         */
        logout: function(scope) {
            return function() {
                $http
                .get(USER_CONFIG.logoutUrl, {})

                .success(function(data, status, headers, config)
                {
                    if ( data.success === true ) {
                        scope.user = Data.user = null;
                    } else {
                        alert('Error while trying to logout');
                    }
                })

                .error(function(data, status, headers, config)
                {
                    alert('Error while trying to logout');
                })
                ;
            };
        }
    };
}])

;