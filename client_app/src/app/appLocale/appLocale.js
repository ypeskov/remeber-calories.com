angular.module('appLocale', ['ngLocale'])

.provider('appLocale', ['APP_LOCALE_CONFIG', function(APP_LOCALE_CONFIG) {
    "use strict";

    var injector = angular.injector(['ngLocale']);
    var $locale = injector.get('$locale');

    return {
        getLocaleId: function() {
            return $locale.id;
        },

        $get: function() { },

        getTemplatePath: function(localeId, moduleName, templateName) {
            return moduleName + '/' + APP_LOCALE_CONFIG.I18N_DIR + '/' + localeId + '/templates/' + templateName + '.tpl.html';
        }
    };
}])

.constant('APP_LOCALE_CONFIG', {
    I18N_DIR:           'i18n',
    DEFAULT_LOCALE:     'en-us'
})

;