/**
 * Created by Yuriy Peskov <yuriy.peskov@gmail.com> on 1/31/14.
 */

angular.module('app.bookmarks', ['ui.router', 'app.topmenu'])

    .config(['$stateProvider', 'TOP_MENU_CONFIG', function($stateProvider, TOP_MENU_CONFIG) {

        //get the current menu item id
        var menuItem = TOP_MENU_CONFIG.items[4];

        $stateProvider.state(menuItem.id, {
            url: menuItem.path,
            views: {
                "main": {
                    controller: 'BookmarksCtrl',
                    templateUrl: menuItem.id + '/' + menuItem.id + '.tpl.html'
                }
            },
            data:{
                pageTitle:  menuItem.label,
                menuItem:   menuItem
            }
        });
    }])

    .controller( 'BookmarksCtrl', ['$scope', '$state', function( $scope, $state ) {

    }])

;