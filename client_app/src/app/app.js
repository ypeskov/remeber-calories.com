angular.module('app', [
    'ngLocale',
    'templates-app',
    'templates-common',
    'ui.router',

    'app.topmenu',
    'app.language',
    'app.user',
    'app.dashboard',
    'app.settings',
    'app.targets',
    'app.progress',
    'app.advices',
    'app.bookmarks'
])

.constant('APP_CONFIG', {
    'RESPONSE_STATUS': {
        'SUCCESS':  200
    }
})


.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/dashboard');
    }])

.controller('AppCtrl', ['$scope', '$location', '$log', '$rootScope', 'Data',
    function($scope, $location, $log, $rootScope, Data) {
        "use strict";
        var _self = this;

        _self.Data = Data;


        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if ( angular.isDefined(toState.data.pageTitle) ) {
                _self.pageTitle = toState.data.pageTitle + ' | Remember Calories';
                Data.showPreloader = false;
            }

            if ( angular.isDefined(toState.data.menuItem) ) {
                _self.menuItem = toState.data.menuItem;
            } else {
                _self.menuItem = {};
            }
        });

        $rootScope.$log = $log;

        return $scope.AppCtrl = _self;
    }])


.factory('Data', [function() {
    "use strict";

    return {
        showPreloader:  true,
        targets:        []
    };

}])


;

