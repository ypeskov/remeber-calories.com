angular.module('app.targets', ['ui.router', 'app.topmenu'])

.constant('APP_TARGETS_CONFIG', {
    MODULE_NAME:        'targets',
    MODULE_MAIN_TEMPLATE:    'targets'
})

.config(['$stateProvider', 'TOP_MENU_CONFIG', 'appLocaleProvider', 'APP_TARGETS_CONFIG',
          function($stateProvider, TOP_MENU_CONFIG, appLocaleProvider, APP_TARGETS_CONFIG) {

    var menuItem        =  TOP_MENU_CONFIG.items[1];
    var currentLocale   = appLocaleProvider.getLocaleId();
//    var moduleName      = 'targets';
//    var templateName    = 'targets';

    $stateProvider.state(menuItem.id, {
        url: menuItem.path,
        views: {
            "main": {
              controller: 'TargetsCtrl',
              //templateUrl: menuItem.id + '/' + menuItem.id + '.tpl.html'
              templateUrl: appLocaleProvider.getTemplatePath(currentLocale,
                                                            APP_TARGETS_CONFIG.MODULE_NAME,
                                                            APP_TARGETS_CONFIG.MODULE_MAIN_TEMPLATE)
            }
        },
        data:{
            pageTitle:  menuItem.label,
            menuItem:   menuItem
        }
    });

}])


.controller('TargetsCtrl', ['$scope', 'TargetsService', 'Data',
                            function($scope, TargetsService, Data) {

    var _self = this;

    _self.Data = Data;
    _self.newTargetPopupShow = false;
    _self.newTarget = {};

    TargetsService.getAll();

    _self.showNewTargetPopup = function() {
        "use strict";

        _self.newTargetPopupShow = ! _self.newTargetPopupShow;
        Data.showPreloader = true;
    };

    _self.hideNewTargetPopup = function() {
        "use strict";

        Data.showPreloader = false;
        _self.newTargetPopupShow = ! _self.newTargetPopupShow;
    };

    _self.addNewTarget = function() {
        "use strict";

        TargetsService.newTarget(_self, _self.newTarget);
    };

    return $scope.TargetsCtrl = _self;
}])


.factory('TargetsService', ['APP_CONFIG', '$http', 'Data', function(APP_CONFIG, $http, Data) {
    "use strict";
    var _self = this;

    return {
        getAll: function() {
            $http
                .get('/targets', {})
                .success(function(data, status, headers, config) {
                    if ( data.success === true ) {
                        Data.targets = data.data;
                    } else {
                        alert('Error in retrieving targets');
                    }

                })
                .error(function(data, status, headers, config) {
                    alert('Error in retrieving targets');
                });
        },

        newTarget: function(controller, newTarget) {
            $http.post('/targets', {
                'newTarget': newTarget
            })
            .success(function(data, status, headers, config) {
                if ( data.isValid !== false ) {
                    controller.newTargetPopupShow   = false;
                    controller.Data.showPreloader   = false;
                }

            })
            .error(function(data, status, headers, config) {

            });
        }
    };
}])

;