angular.module('templates-app', ['advices/advices.tpl.html', 'bookmarks/bookmarks.tpl.html', 'dashboard/i18n/en-us/templates/dashboard-anonymous.tpl.html', 'dashboard/i18n/en-us/templates/dashboard-user.tpl.html', 'dashboard/i18n/en-us/templates/dashboard.tpl.html', 'dashboard/i18n/ru/templates/dashboard-anonymous.tpl.html', 'dashboard/i18n/ru/templates/dashboard-user.tpl.html', 'dashboard/i18n/ru/templates/dashboard.tpl.html', 'dashboard/i18n/uk/templates/dashboard-anonymous.tpl.html', 'dashboard/i18n/uk/templates/dashboard-user.tpl.html', 'dashboard/i18n/uk/templates/dashboard.tpl.html', 'language/i18n/en-us/language.tpl.html', 'progress/progress.tpl.html', 'settings/settings.tpl.html', 'targets/i18n/en-us/templates/targets.tpl.html', 'targets/i18n/ru/templates/targets.tpl.html', 'topmenu/topmenu.tpl.html', 'user/user_section.tpl.html']);

angular.module("advices/advices.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("advices/advices.tpl.html",
    "<h2>Advices page</h2>");
}]);

angular.module("bookmarks/bookmarks.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("bookmarks/bookmarks.tpl.html",
    "<h2>Bookmarks page</h2>");
}]);

angular.module("dashboard/i18n/en-us/templates/dashboard-anonymous.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("dashboard/i18n/en-us/templates/dashboard-anonymous.tpl.html",
    "<h3>You are not logged in</h3>\n" +
    "\n" +
    "<h4>Welcome to the best service.</h4>");
}]);

angular.module("dashboard/i18n/en-us/templates/dashboard-user.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("dashboard/i18n/en-us/templates/dashboard-user.tpl.html",
    "<h3>Targets:</h3>\n" +
    "<ul>\n" +
    "    <li data-ng-repeat=\"target in DashboardUserCtrl.Data.targets | filter: {active: 1}\">\n" +
    "        <a href=\"#\">{{ target.name }}</a>\n" +
    "    </li>\n" +
    "</ul>");
}]);

angular.module("dashboard/i18n/en-us/templates/dashboard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("dashboard/i18n/en-us/templates/dashboard.tpl.html",
    "<h2>Dashboard-qqq</h2>\n" +
    "\n" +
    "<div data-ui-view=\"main\"></div>");
}]);

angular.module("dashboard/i18n/ru/templates/dashboard-anonymous.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("dashboard/i18n/ru/templates/dashboard-anonymous.tpl.html",
    "К сожалению, вы еще не авторизовались на сервисе.\n" +
    "Проследуйте - сюда -");
}]);

angular.module("dashboard/i18n/ru/templates/dashboard-user.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("dashboard/i18n/ru/templates/dashboard-user.tpl.html",
    "<h3>Ваши активные цели:</h3>\n" +
    "<ul>\n" +
    "    <li data-ng-repeat=\"target in DashboardUserCtrl.Data.targets | filter: {active: 1}\">\n" +
    "        <a href=\"#\">{{ target.name }}</a>\n" +
    "    </li>\n" +
    "</ul>");
}]);

angular.module("dashboard/i18n/ru/templates/dashboard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("dashboard/i18n/ru/templates/dashboard.tpl.html",
    "<h2>Начало всех начал</h2>\n" +
    "\n" +
    "<div data-ui-view=\"main\"></div>");
}]);

angular.module("dashboard/i18n/uk/templates/dashboard-anonymous.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("dashboard/i18n/uk/templates/dashboard-anonymous.tpl.html",
    "К сожалению, вы еще не авторизовались на сервисе.\n" +
    "Проследуйте - сюда -");
}]);

angular.module("dashboard/i18n/uk/templates/dashboard-user.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("dashboard/i18n/uk/templates/dashboard-user.tpl.html",
    "<h3>Ваши активные цели:</h3>\n" +
    "<ul>\n" +
    "    <li data-ng-repeat=\"target in DashboardUserCtrl.Data.targets | filter: {active: 1}\">\n" +
    "        <a href=\"#\">{{ target.name }}</a>\n" +
    "    </li>\n" +
    "</ul>");
}]);

angular.module("dashboard/i18n/uk/templates/dashboard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("dashboard/i18n/uk/templates/dashboard.tpl.html",
    "<h2>Файний початок</h2>\n" +
    "\n" +
    "<div data-ui-view=\"main\"></div>");
}]);

angular.module("language/i18n/en-us/language.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("language/i18n/en-us/language.tpl.html",
    "<select data-ng-model=\"LanguageCtrl.currentLocale\"\n" +
    "        data-ng-options=\"lang.label for lang in LanguageCtrl.locales\"\n" +
    "        data-ng-change=\"LanguageCtrl.changeLocale()\">\n" +
    "</select>");
}]);

angular.module("progress/progress.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("progress/progress.tpl.html",
    "<h2>Progress page</h2>");
}]);

angular.module("settings/settings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("settings/settings.tpl.html",
    "<h2>Settings page</h2>");
}]);

angular.module("targets/i18n/en-us/templates/targets.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("targets/i18n/en-us/templates/targets.tpl.html",
    "<h3>\n" +
    "    <button id=\"add_new-target_button\" data-ng-click=\"TargetsCtrl.showNewTargetPopup()\">Add a new target</button>\n" +
    "</h3>\n" +
    "<hr />\n" +
    "\n" +
    "<h3>Current Targets:</h3>\n" +
    "\n" +
    "<ul>\n" +
    "    <li data-ng-repeat=\"target in TargetsCtrl.Data.targets | filter: {active: 1}\">\n" +
    "        <a href=\"#\">{{ target.name }}</a>\n" +
    "    </li>\n" +
    "</ul>\n" +
    "\n" +
    "<hr />\n" +
    "\n" +
    "<h3>All targets:</h3>\n" +
    "<ul>\n" +
    "    <li data-ng-repeat=\"target in TargetsCtrl.Data.targets\">\n" +
    "        <a href=\"#\">{{ target.name }}</a>\n" +
    "    </li>\n" +
    "</ul>\n" +
    "\n" +
    "<hr />\n" +
    "\n" +
    "<h3>Archived targets:</h3>\n" +
    "<ul>\n" +
    "    <li data-ng-repeat=\"target in TargetsCtrl.Data.targets | filter: {archived: 1}\">\n" +
    "        <a href=\"#\">{{ target.name }}</a>\n" +
    "    </li>\n" +
    "</ul>\n" +
    "\n" +
    "\n" +
    "\n" +
    "<div id=\"add_target_container\" class=\"modalPopup\" data-ng-show=\"TargetsCtrl.newTargetPopupShow\">\n" +
    "    <div class=\"add_new_target_label\"><label for=\"add_new_target_name\">Name:&nbsp;</label></div>\n" +
    "    <input class=\"add_new_target_input\" id=\"add_new_target_name\" type=\"text\" data-ng-model=\"TargetsCtrl.newTarget.name\" />\n" +
    "    <div class=\"separator1\"></div>\n" +
    "\n" +
    "    <div class=\"add_new_target_label\"><label for=\"add_new_target_description\">Description:&nbsp;</label></div>\n" +
    "    <textarea class=\"add_new_target_input\" id=\"add_new_target_description\" data-ng-model=\"TargetsCtrl.newTarget.description\"></textarea>\n" +
    "    <div class=\"separator1\"></div>\n" +
    "\n" +
    "    <div class=\"add_new_target_label\"><label for=\"add_new_target_startdate\">Start Date:&nbsp;</label></div>\n" +
    "    <input class=\"add_new_target_input\" id=\"add_new_target_startdate\" type=\"text\" data-ng-model=\"TargetsCtrl.newTarget.date_start\" />\n" +
    "    <div class=\"separator1\"></div>\n" +
    "\n" +
    "    <div class=\"add_new_target_label\"><label for=\"add_new_target_finishdate\">Start Date:&nbsp;</label></div>\n" +
    "    <input class=\"add_new_target_input\" id=\"add_new_target_finishdate\" type=\"text\" data-ng-model=\"TargetsCtrl.newTarget.date_finish\" />\n" +
    "    <div class=\"separator1\"></div>\n" +
    "\n" +
    "    <input type=\"button\" data-ng-click=\"TargetsCtrl.addNewTarget()\" value=\"Add Target\" />\n" +
    "    <input type=\"button\" data-ng-click=\"TargetsCtrl.hideNewTargetPopup()\" value=\"Cancel\" />\n" +
    "\n" +
    "</div>");
}]);

angular.module("targets/i18n/ru/templates/targets.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("targets/i18n/ru/templates/targets.tpl.html",
    "<h3>\n" +
    "    <button id=\"add_new-target_button\" data-ng-click=\"TargetsCtrl.showNewTargetPopup()\">Add a new target</button>\n" +
    "</h3>\n" +
    "<hr />\n" +
    "\n" +
    "<h3>Current Targets:</h3>\n" +
    "\n" +
    "<ul>\n" +
    "    <li data-ng-repeat=\"target in TargetsCtrl.Data.targets | filter: {active: 1}\">\n" +
    "        <a href=\"#\">{{ target.name }}</a>\n" +
    "    </li>\n" +
    "</ul>\n" +
    "\n" +
    "<hr />\n" +
    "\n" +
    "<h3>All targets:</h3>\n" +
    "<ul>\n" +
    "    <li data-ng-repeat=\"target in TargetsCtrl.Data.targets\">\n" +
    "        <a href=\"#\">{{ target.name }}</a>\n" +
    "    </li>\n" +
    "</ul>\n" +
    "\n" +
    "<hr />\n" +
    "\n" +
    "<h3>Archived targets:</h3>\n" +
    "<ul>\n" +
    "    <li data-ng-repeat=\"target in TargetsCtrl.Data.targets | filter: {archived: 1}\">\n" +
    "        <a href=\"#\">{{ target.name }}</a>\n" +
    "    </li>\n" +
    "</ul>\n" +
    "\n" +
    "\n" +
    "\n" +
    "<div id=\"add_target_container\" class=\"modalPopup\" data-ng-show=\"TargetsCtrl.newTargetPopupShow\">\n" +
    "    <div class=\"add_new_target_label\"><label for=\"add_new_target_name\">Name:&nbsp;</label></div>\n" +
    "    <input class=\"add_new_target_input\" id=\"add_new_target_name\" type=\"text\" data-ng-model=\"TargetsCtrl.newTarget.name\" />\n" +
    "    <div class=\"separator1\"></div>\n" +
    "\n" +
    "    <div class=\"add_new_target_label\"><label for=\"add_new_target_description\">Description:&nbsp;</label></div>\n" +
    "    <textarea class=\"add_new_target_input\" id=\"add_new_target_description\" data-ng-model=\"TargetsCtrl.newTarget.description\"></textarea>\n" +
    "    <div class=\"separator1\"></div>\n" +
    "\n" +
    "    <div class=\"add_new_target_label\"><label for=\"add_new_target_startdate\">Start Date:&nbsp;</label></div>\n" +
    "    <input class=\"add_new_target_input\" id=\"add_new_target_startdate\" type=\"text\" data-ng-model=\"TargetsCtrl.newTarget.date_start\" />\n" +
    "    <div class=\"separator1\"></div>\n" +
    "\n" +
    "    <div class=\"add_new_target_label\"><label for=\"add_new_target_finishdate\">Start Date:&nbsp;</label></div>\n" +
    "    <input class=\"add_new_target_input\" id=\"add_new_target_finishdate\" type=\"text\" data-ng-model=\"TargetsCtrl.newTarget.date_finish\" />\n" +
    "    <div class=\"separator1\"></div>\n" +
    "\n" +
    "    <input type=\"button\" data-ng-click=\"TargetsCtrl.addNewTarget()\" value=\"Add Target\" />\n" +
    "    <input type=\"button\" data-ng-click=\"TargetsCtrl.hideNewTargetPopup()\" value=\"Cancel\" />\n" +
    "\n" +
    "</div>");
}]);

angular.module("topmenu/topmenu.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("topmenu/topmenu.tpl.html",
    "<div id=\"topmenu_container\" data-ng-controller=\"TopmenuCtrl\">\n" +
    "    <ul>\n" +
    "        <li data-ng-repeat=\"menuItem in menu\">\n" +
    "            <a href=\"#{{ menuItem.path }}\">{{ menuItem.label }}</a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</div>");
}]);

angular.module("user/user_section.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("user/user_section.tpl.html",
    "<div>\n" +
    "    <div data-ng-show=\"UserCtrl.user.identified\">\n" +
    "        <span>Welcome back,</span>{{ UserCtrl.user.first_name }}&nbsp;\n" +
    "        <a href=\"#\" data-ng-click=\"UserCtrl.logout()\">Logout</a>\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-ng-show=\"! UserCtrl.user.identified\">\n" +
    "        <form>\n" +
    "            <input id=\"user_email\" type=\"email\" data-ng-model=\"UserCtrl.user.email\" autocomplete=\"on\" />\n" +
    "            <input type=\"password\" data-ng-model=\"UserCtrl.user.password\" />\n" +
    "            <input type=\"submit\" data-ng-click=\"UserCtrl.tryToLogin();\" value=\"Log In\" />\n" +
    "        </form>\n" +
    "\n" +
    "        <br />\n" +
    "\n" +
    "        <div class=\"has-error\" data-ng-show=\"UserCtrl.displayLoginError\">\n" +
    "            <span class=\"help-block\">Error logging into account</span>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>");
}]);
