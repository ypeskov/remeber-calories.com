angular.module('app.settings', ['ui.router', 'app.topmenu'])

.config(['$stateProvider', 'TOP_MENU_CONFIG', function($stateProvider, TOP_MENU_CONFIG) {

    //get the current menu item id
    var menuItem = TOP_MENU_CONFIG.items[5];

    $stateProvider.state(menuItem.id, {
        url: menuItem.path,
        views: {
            "main": {
              controller: 'SettingsCtrl',
              templateUrl: menuItem.id + '/' + menuItem.id + '.tpl.html'
            }
        },
        data:{
            pageTitle:  menuItem.label,
            menuItem:   menuItem
        }
    });
}])

.controller( 'SettingsCtrl', ['$scope', '$state', function( $scope, $state ) {

}])

;
