angular.module( 'app.dashboard', [
    'ui.router',

    'appLocale',
    'app.topmenu',
    'app.user'
])



.config(['$stateProvider', 'TOP_MENU_CONFIG', 'appLocaleProvider',
        function config( $stateProvider, TOP_MENU_CONFIG, appLocaleProvider) {

    var currentLocale = appLocaleProvider.getLocaleId();
    var moduleName = 'dashboard';

    //get the current menu item id
    var menuItem =  TOP_MENU_CONFIG.items[0];

    $stateProvider
    .state(menuItem.id, {
        url:    '/dashboard',
        reloadOnSearch: false,
        views: {
            "main": {
                controller: 'DashboardCtrl',
                templateUrl: appLocaleProvider.getTemplatePath(currentLocale, moduleName, 'dashboard')
            }
        },
        data:{
            pageTitle:  menuItem.label,
            menuItem:   menuItem
        }
    })

    .state('dashboard.anonymous', {
        url: '-anonymous',
        views: {
            "main": {
                controller:     'DashboardAnonCtrl',
                templateUrl: appLocaleProvider.getTemplatePath(currentLocale, moduleName, 'dashboard-anonymous')
            }
        },
        data: {
            pageTitle:  'Unauthorized user',
            menuItem:   menuItem
        }
    })

    .state('dashboard.user', {
        url:    '-user',
        views: {
            "main": {
                controller:     'DashboardUserCtrl',
                templateUrl: appLocaleProvider.getTemplatePath(currentLocale, moduleName, 'dashboard-user')
            }
        },
        data: {
            pageTitle:  'Dashboard',
            menuItem:   menuItem
        }
    })

    ;
}])

.controller( 'DashboardCtrl',
        ['$scope', 'Data', '$state', function( $scope, Data, $state ) {

    "use strict";
    var _self = this;

    if ( typeof Data.user == 'undefined' || Data.user.id == null ) {
        $state.go('dashboard.anonymous');
    } else {
        $state.go('dashboard.user');
    }

    return $scope.DashboardCtrl = _self;
}])

.controller('DashboardAnonCtrl', ['$scope', function($scope) {
    "use strict";
    var _self = this;

    return $scope.DashboardAnonCtrl = _self;
}])

.controller('DashboardUserCtrl',
            ['$scope', 'TargetsService', 'Data',
            function($scope, TargetsService, Data) {

    "use strict";
    var _self = this;

    _self.Data = Data;

    TargetsService.getAll();

    return $scope.DashboardUserCtrl = _self;
}])


.factory('Dashboard', ['$http', function($http) {
    "use strict";
    var _self = this;

    return {

    };
}])



;

