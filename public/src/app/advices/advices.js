angular.module('app.advices', ['ui.router', 'app.topmenu'])

.config(['$stateProvider', 'TOP_MENU_CONFIG', function($stateProvider, TOP_MENU_CONFIG) {

    //get the current menu item id
    var menuItem = TOP_MENU_CONFIG.items[3];

    $stateProvider.state(menuItem.id, {
        url: menuItem.path,
        views: {
            "main": {
                controller: 'AdvicesCtrl',
                templateUrl: menuItem.id + '/' + menuItem.id + '.tpl.html'
            }
        },
        data:{
            pageTitle:  menuItem.label,
            menuItem:   menuItem
        }
    });
}])

.controller( 'AdvicesCtrl', ['$scope', '$state', function( $scope, $state ) {

}])

;