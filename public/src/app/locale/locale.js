/**
 * remember-calories.com (c) 2010-2014
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 2/10/14
 * Time: 9:17 AM
 */

angular.module('locale', [])

.provider('locale', [ function() {
    "use strict";

    var defaultLocale = 'en';

    return {
        /**
         * Returns a string for the for template loading depending on locale
         *
         * @param String locale
         * @param String tplRoot
         * @param String tplSuffix
         * @returns {string}
         */
        getTemplatePath: function (locale, tplRoot, tplSuffix)
        {
            //if locale is not set then think about it as a default
            if ( locale == null ) {
                locale = defaultLocale;
            }

            var path = tplRoot + '/' + tplRoot + '.tpl.html';

            // if locale is not a default one then build full path from templates directories
            if ( locale !== defaultLocale ) {
                path = tplRoot + '/i18n/templates/' + locale + '/' + tplRoot + tplSuffix + '.tpl.html';
            } else {
                path = tplRoot + '/' + tplRoot + tplSuffix + '.tpl.html';
            }

            return path;
        },

        $get: function() {
        }
    };
}])

;