/**
 * remember-calories.com (c) 2010-2014
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 2/20/14
 * Time: 9:21 AM
 */

angular.module('qqq', [])

.config()

.controller('LanguageCtrl', ['$scope', function($scope) {
    "use strict";

    var _self = this;

    _self.languages = [
        'English',
        'Russian'
    ];

    return $scope.LanguageCtrl = _self;
}])

;