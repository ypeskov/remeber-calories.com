angular.module('app.progress', [])

.config(['$stateProvider', 'TOP_MENU_CONFIG', function($stateProvider, TOP_MENU_CONFIG) {

    //get the current menu item id
    var menuItem = TOP_MENU_CONFIG.items[2];

    $stateProvider.state(menuItem.id, {
        url: menuItem.path,
        views: {
            "main": {
                controller: 'SettingsCtrl',
                templateUrl: menuItem.id + '/' + menuItem.id + '.tpl.html'
            }
        },
        data:{
            pageTitle:  menuItem.label,
            menuItem:   menuItem
        }
    });
}])

.controller('ProgressCtrl', ['$scope', '$state', function( $scope, $state ) {

}])

;